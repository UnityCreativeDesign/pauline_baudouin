﻿/// <summary>
/// The class representing the objects we want to spawn
/// The variable names must strictly respect the one in the JSON
/// </summary>

[System.Serializable]
public class SpawnObject
{
    public string name;
    public Position position;
    public Rotation rotation;
    public Scale scale;
    public string primitive;
    public Information information;
}