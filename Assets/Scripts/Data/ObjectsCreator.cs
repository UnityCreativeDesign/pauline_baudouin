﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Component dedicated to reading the JSON file and creating the GameObject accordingly to its given values
/// </summary>
public class ObjectsCreator : MonoBehaviour
{
    /// <summary>
    /// A reference to the path to the JSON file
    /// </summary>
    [SerializeField]
    private string _pathToJSON;

    private void Awake()
    {
        // First we transform the JSON into an object we can manipulate in code
        ParentObject rootObject = JsonUtility.FromJson<ParentObject>(File.ReadAllText(Application.streamingAssetsPath + "/" + _pathToJSON));

        // Foreach spawn object in the rooted object
        foreach(SpawnObject currentSpawnObject in rootObject.spawnObjects)
        {
            // If the primitive type given corresponds to a primitive type known by unity
            if (Enum.TryParse(currentSpawnObject.primitive, out PrimitiveType primitiveType))
            {
                // Create the new object and set this transform as parent
                var newlyCreatedTransform = GameObject.CreatePrimitive(primitiveType).transform;

                // Setup all the relevant values
                newlyCreatedTransform.SetParent(transform);
                newlyCreatedTransform.gameObject.name = currentSpawnObject.name;
                newlyCreatedTransform.position = currentSpawnObject.position.ToVector3();
                newlyCreatedTransform.rotation = Quaternion.Euler(currentSpawnObject.rotation.ToVector3());
                newlyCreatedTransform.localScale = currentSpawnObject.scale.ToVector3();

                // Add the component that will help us visualising the data
                newlyCreatedTransform.gameObject.AddComponent<InspectorInformation>().ObjectValues = currentSpawnObject;
            }
            else
                Debug.Log("Can't find primitive type for " + currentSpawnObject.primitive);
        }
    }
}
