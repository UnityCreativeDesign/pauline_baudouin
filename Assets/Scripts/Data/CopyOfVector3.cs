﻿using UnityEngine;

/// <summary>
/// Abstract class representing the structure of a Vector3
/// It is necessary to automatise the reading of the JSON file by JsonUtility
/// </summary>
public abstract class CopyOfVector3
{
    public double x;
    public double y;
    public double z;

    /// <summary>
    /// Function converting this object into a Vector3
    /// </summary>
    /// <returns></returns>
    public Vector3 ToVector3()
    {
        return new Vector3((float)x, (float)y, (float)z);
    }
}