﻿/// <summary>
/// The class representing the position of the object we want to spawn
/// The variable names must strictly respect the one in the JSON
/// </summary>

[System.Serializable]
public class Position : CopyOfVector3 { }