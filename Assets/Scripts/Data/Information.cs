﻿/// <summary>
/// The class representing some information about the object we want to spawn
/// The variable names must strictly respect the one in the JSON
/// </summary>

[System.Serializable]
public class Information
{
    public string stringField;
    public int intField;
    public double floatField;
    public bool boolField;
}