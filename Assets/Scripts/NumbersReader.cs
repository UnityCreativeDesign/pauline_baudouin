﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Script taking care of reading any file containing numbers and doing all sorts of computation on the newly created list
/// </summary>
public class NumbersReader : MonoBehaviour
{
    /// <summary>
    /// A serialised string to store the path of the file we want to read from the streaming asset folder
    /// </summary>
    [SerializeField]
    private string _pathToFile;

    /// <summary>
    /// The list of numbers
    /// </summary>
    private List<int> _listOfNumbers = new List<int>();

    private void Awake()
    {
        ReadFileAndStoreNumbers();
        ArrangeLowestToHighest();
        PrintMultiplesOf(2);
        PrintAllPrimeNumbers();
    }

    /// <summary>
    /// Funciton reading the file Numbers.txt and storing the numbers into a list
    /// </summary>
    private void ReadFileAndStoreNumbers()
    {
        // For each line in the file
        foreach(string line in File.ReadLines(Application.streamingAssetsPath + "/" + _pathToFile))
        {
            // Add them in the list.
            // I consider that we don't need to check if the line is actually what we expect 
            // but we could also do a check first with the TryParse function
            _listOfNumbers.Add(int.Parse(line));
        }
    }

    /// <summary>
    /// Function arranging the list from lowest to highest
    /// </summary>
    private void ArrangeLowestToHighest()
    {
        // Creating the arranged list and adding the first element of the _listOfNumber to have something to compare the following numbers to
        List<int> arrangedList = new List<int>();
        arrangedList.Add(_listOfNumbers[0]);

        // for each item in the list of numbers
        for (int j = 1; j < _listOfNumbers.Count; j++)
        {
            var currentIntValue = _listOfNumbers[j];
            // If it is lower or equal to the first item of the arranged list,
            if (currentIntValue <= arrangedList[0])
            {
                // Storing the current state of arrangedList
                var tempList = arrangedList;
                // Resetting the arranged list
                arrangedList = new List<int>();
                // Adding the current value to the list as first item
                arrangedList.Add(currentIntValue);
                // Adding the rest of the list
                arrangedList.AddRange(tempList);
                continue;
            }

            // If the current value is grater or equal to the last item in the list
            if (currentIntValue >= arrangedList[arrangedList.Count - 1])
            {
                // Simply adding the value to the list
                arrangedList.Add(currentIntValue);
                continue;
            }

            // If it is somewhere in the list, find its position
            int position = GetPosition();

            // Collect the list before and after the position 
            List<int> beforeList = arrangedList.GetRange(0, position);
            List<int> afterList = arrangedList.GetRange(position, arrangedList.Count - position);

            // Reset list and add the different elements in the order
            arrangedList = new List<int>();
            arrangedList.AddRange(beforeList);
            arrangedList.Add(currentIntValue);
            arrangedList.AddRange(afterList);

            // Function returning the index at which the currentIntValue should be added to the arrangedList
            int GetPosition()
            {
                // For i going from 1 to the last but one index in the arranged list
                for (int i = 1; i < arrangedList.Count - 1; i++)
                {
                    // If the currentIntValue is smaller or equal to the current value of arranged list AND greather than the next one
                    if (currentIntValue <= arrangedList[i] && currentIntValue > arrangedList[i - 1])
                        // Returning the current index
                        return i;
                }
                // If we couldn't find the position, returning -1
                Debug.Log("Couldn't find the position of item " + currentIntValue);
                return -1;
            }
        }
        _listOfNumbers = arrangedList;
    }

    /// <summary>
    /// Function printing all the multiples of a given number
    /// </summary>
    /// <param name="reference"></param>
    private void PrintMultiplesOf(int reference)
    {
        PrintListCompact("All multiples of " + reference + ":\n", GetMultiplesOf(reference));
    }

    /// <summary>
    /// Function printing all the prime numbers of the _listOfNumbers
    /// </summary>
    private void PrintAllPrimeNumbers()
    {
        // Creating a copy of the list of numbers to which we will remove all the multiples that we find
        var finalList = _listOfNumbers;
        // For i going from 2 to the max value of the list (ordered list)
        for(int i = 2; i < _listOfNumbers[_listOfNumbers.Count - 1]; i++)
        {
            // For each multiple of i
            foreach(var multiple in GetMultiplesOf(i))
            {
                // If it is different from i, remove it from the list
                // That is because a primary number is allowed to be dividible by itself
                if(i != multiple)
                    finalList.Remove(multiple);
            }
        }
        PrintListCompact("All prime numbers in list: ", finalList);
    }

    /// <summary>
    /// Function printing a list in its compact version
    /// </summary>
    /// <param name="title">The text we want to put before displaying the list</param>
    /// <param name="toPrint">The list to print</param>
    private void PrintListCompact(string title, List<int> toPrint)
    {
        Debug.Log(title);
        string result = "";
        // Add each entry of the list to the var result
        foreach (var element in toPrint)
            result += element + " ";
        Debug.Log(result);
    }

    /// <summary>
    /// Function printing all the multiples of the given int
    /// </summary>
    /// <param name="reference"></param>
    private List<int> GetMultiplesOf(int reference)
    {
        //Debug.Log("Printing all the multiples of " + reference + " in the list:");
        List<int> resultList = new List<int>();
        // For each item in the list
        foreach(float currentValue in _listOfNumbers)
        {
            var floatResult = currentValue / reference;
            // If the division of the currentValue by the reference gives the same value as one of the closest ints, we print its value
            if (floatResult == Mathf.Ceil(floatResult) || floatResult == Mathf.Floor(floatResult))
                resultList.Add((int)currentValue);
        }
        return resultList;
    }
}
