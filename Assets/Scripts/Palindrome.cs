﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script checking if the given string is a palindrome or not
/// </summary>
public class Palindrome : MonoBehaviour
{
    /// <summary>
    /// The string we want to test 
    /// </summary>
    [SerializeField]
    private string _isPalindrome;

    /// <summary>
    /// Boolean must be set to true if we want to consider spaces as part of the palindrome, false if we must ignore them
    /// For example "no lemon no melon" won't be considered a palindrome if _spaceSensitive is true
    /// </summary>
    [SerializeField]
    private bool _spaceSentitive;

    private void Awake()
    {
        string linkWord = IsPalindromeRecursive(_spaceSentitive ? _isPalindrome : _isPalindrome.Replace(" ", "")) ? " is " : " is not ";
        Debug.Log(_isPalindrome + linkWord + "a palindrome");
    }

    /// <summary>
    /// Recursive function returning true if the given string is a palindrome
    /// </summary>
    /// <param name="toCheck"></param>
    /// <returns></returns>
    private bool IsPalindromeRecursive(string toCheck)
    {
        // If we reached the center of the word it means the word is a palindrome
        if (toCheck.Length <= 1)
            return true;

        // If the comparison between the first char and the last char is equal to null AKA if they are strictly equal
        if (toCheck[0].CompareTo(toCheck[toCheck.Length - 1]) == 0)
            // We recursively call this function and return its result
            return IsPalindromeRecursive(toCheck.Substring(1, toCheck.Length - 2));
        else
            // If at some point we reach this statement, it means that two characters are not exactly equal so the whole word is not a palindrome
            return false;
    }
}
