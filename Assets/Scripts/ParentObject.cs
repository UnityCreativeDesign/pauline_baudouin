﻿using System.Collections.Generic;

/// <summary>
/// Classes representing the JSON file
/// The variable names must strictly respect the one in the JSON
/// </summary>

[System.Serializable]
public class ParentObject
{
    public List<SpawnObject> spawnObjects;
}

