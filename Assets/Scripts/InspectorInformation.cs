﻿using UnityEngine;

/// <summary>
/// Component used to display all the info concerning a SpawnObject in the inspector
/// </summary>
public class InspectorInformation : MonoBehaviour
{
    /// <summary>
    /// A reference to the object containing the values we want to display
    /// </summary>
    public SpawnObject ObjectValues;
}
